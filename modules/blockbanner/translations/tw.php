<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockbanner}prestashop>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = '橫幅區塊';
$_MODULE['<{blockbanner}prestashop>blockbanner_9d9becee392c0fbcc66ff4981b8ae2f7'] = '在店铺頁面顶部显示一个横幅.';
$_MODULE['<{blockbanner}prestashop>blockbanner_126b21ce46c39d12c24058791a236777'] = '圖片有誤';
$_MODULE['<{blockbanner}prestashop>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = '上傳檔案時發生錯誤';
$_MODULE['<{blockbanner}prestashop>blockbanner_efc226b17e0532afff43be870bff0de7'] = '設定已經成功更新';
$_MODULE['<{blockbanner}prestashop>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = '設定';
$_MODULE['<{blockbanner}prestashop>blockbanner_89ca5c48bbc6b7a648a5c1996767484c'] = '圖片區塊';
$_MODULE['<{blockbanner}prestashop>blockbanner_296dce46079fc6cabe69b7e7edb25506'] = '您可以選擇上傳圖片文件或在下面的“圖片鏈接”選項輸入其圖片鏈接。';
$_MODULE['<{blockbanner}prestashop>blockbanner_9ce38727cff004a058021a6c7351a74a'] = '圖片連結';
$_MODULE['<{blockbanner}prestashop>blockbanner_5b79f7f033924a348f025924820988cb'] = '您可以選擇輸入圖片的絕對鏈接或上傳圖片文件到上面的"圖片區塊"選項.';
$_MODULE['<{blockbanner}prestashop>blockbanner_18f2ae2bda9a34f06975d5c124643168'] = '圖片說明';
$_MODULE['<{blockbanner}prestashop>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = '請為橫幅(banner)輸入一個简短而具意義的描述.';
$_MODULE['<{blockbanner}prestashop>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{blockbanner}prestashop>form_92fbf0e5d97b8afd7e73126b52bdc4bb'] = '選擇檔案';


return $_MODULE;
