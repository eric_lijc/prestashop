<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockspecials}prestashop>blockspecials_c19ed4ea98cbf319735f6d09bde6c757'] = '特價產品區塊';
$_MODULE['<{blockspecials}prestashop>blockspecials_b15e7271053fe9dd22d80db100179085'] = '此模組必需掛接在一個欄位而你的版型不讓執行.';
$_MODULE['<{blockspecials}prestashop>blockspecials_c888438d14855d7d96a2724ee9c306bd'] = '設定已更新';
$_MODULE['<{blockspecials}prestashop>blockspecials_f4f70727dc34561dfde1a3c529b6205c'] = '設定';
$_MODULE['<{blockspecials}prestashop>blockspecials_24ff4e4d39bb7811f6bdf0c189462272'] = '區塊永遠顯示';
$_MODULE['<{blockspecials}prestashop>blockspecials_53d61d1ac0507b1bd8cd99db8d64fb19'] = '無新產品時仍然顯示區塊';
$_MODULE['<{blockspecials}prestashop>blockspecials_00d23a76e43b46dae9ec7aa9dcbebb32'] = '啟用';
$_MODULE['<{blockspecials}prestashop>blockspecials_b9f5c797ebbf55adccdd8539a65a0241'] = '停用';
$_MODULE['<{blockspecials}prestashop>blockspecials_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{blockspecials}prestashop>blockspecials_d1aa22a3126f04664e0fe3f598994014'] = '特價產品';
$_MODULE['<{blockspecials}prestashop>blockspecials_b4f95c1ea534936cc60c6368c225f480'] = '所有特價產品';
$_MODULE['<{blockspecials}prestashop>blockspecials_3c9f5a6dc6585f75042bd4242c020081'] = '暫無特價產品';


return $_MODULE;
