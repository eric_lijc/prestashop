<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockadvertising}prestashop>blockadvertising_bedd646b07e65f588b06f275bd47be07'] = '廣告區塊';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_dc23a0e1424eda56d0700f7ebe628c78'] = '在您的網站選擇區域加入一個廣告區塊.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_b15e7271053fe9dd22d80db100179085'] = '此模組必需掛接在一個欄位而你的版型不讓執行.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_070e16b4f77b90e802f789b5be583cfa'] = '檔案上傳錯誤';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_254f642527b45bc260048e30704edb39'] = '設定';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_89ca5c48bbc6b7a648a5c1996767484c'] = '圖片區塊';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_05177d68c546e5b754e39ae3ce211cc2'] = '圖片將以 155pixels X 163pixels 顯示.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_9ce38727cff004a058021a6c7351a74a'] = '圖片連結';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_b78a3223503896721cca1303f776159b'] = '標題';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_c9cc8cce247e49bae79f15173ce97354'] = '儲存';


return $_MODULE;
