<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockfacebook}prestashop>blockfacebook_06d770829707bb481258791ca979bd2a'] = 'Facebook 模块';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f66e16efd2f1f43944e835aa527f9da8'] = '显示订阅到你的Facebook模块。';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = '配置更新';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = '设置';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = '保存';
$_MODULE['<{blockfacebook}prestashop>preview_31fde7b05ac8952dacf4af8a704074ec'] = '预览';


return $_MODULE;
