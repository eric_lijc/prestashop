<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockfacebook}prestashop>blockfacebook_06d770829707bb481258791ca979bd2a'] = '臉書(Facebook)區塊';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f66e16efd2f1f43944e835aa527f9da8'] = '顯示訂閱你的脸書(Facebook)頁面區塊';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = '設定已更新';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = '設定';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c98cf18081245e342d7929c117066f0b'] = '脸書(Facebook)鏈接(需完整的URL)';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{blockfacebook}prestashop>preview_31fde7b05ac8952dacf4af8a704074ec'] = '預覽';


return $_MODULE;
