<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statssales}prestashop>statssales_45c4b3e103155326596d6ccd2fea0f25'] = '销售与订单';
$_MODULE['<{statssales}prestashop>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = '向导';
$_MODULE['<{statssales}prestashop>statssales_ecfa88580e342ade21a8403765c0dde6'] = '在你的后台，你可以找到以下的订单状态：等待支票付款，接受的付款方式，准备在进步，运输，交付，取消，退款，支付错误，缺货等待银行电汇付款。';
$_MODULE['<{statssales}prestashop>statssales_c3987e4cac14a8456515f0d200da04ee'] = '所有国家';
$_MODULE['<{statssales}prestashop>statssales_d7778d0c64b6ba21494c97f77a66885a'] = '过滤';
$_MODULE['<{statssales}prestashop>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = '成功下单';
$_MODULE['<{statssales}prestashop>statssales_156e5c5872c9af24a5c982da07a883c2'] = '成功购买产品';
$_MODULE['<{statssales}prestashop>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'CSV导出';
$_MODULE['<{statssales}prestashop>statssales_da80af4de99df74dd59e665adf1fac8f'] = '期间没有订单';
$_MODULE['<{statssales}prestashop>statssales_bb7ad89807bf69ddca986c142311f936'] = '产品和订单';
$_MODULE['<{statssales}prestashop>statssales_7442e29d7d53e549b78d93c46b8cdcfc'] = '订单';
$_MODULE['<{statssales}prestashop>statssales_b52b44c9d23e141b067d7e83b44bb556'] = '产品';


return $_MODULE;
