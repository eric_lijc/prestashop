<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statssales}prestashop>statssales_45c4b3e103155326596d6ccd2fea0f25'] = '銷售與訂單';
$_MODULE['<{statssales}prestashop>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = '導引';
$_MODULE['<{statssales}prestashop>statssales_ecfa88580e342ade21a8403765c0dde6'] = '在您的後台，您可以修改以下訂單狀態：Awaiting Check Payment, Payment Accepted, Preparation in Progress, Shipping, Delivered, Cancelled, Refund, Payment Error, Out of Stock, and Awaiting Bank Wire Payment.';
$_MODULE['<{statssales}prestashop>statssales_c3987e4cac14a8456515f0d200da04ee'] = '所有國家';
$_MODULE['<{statssales}prestashop>statssales_d7778d0c64b6ba21494c97f77a66885a'] = '過濾';
$_MODULE['<{statssales}prestashop>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = '已下訂單';
$_MODULE['<{statssales}prestashop>statssales_156e5c5872c9af24a5c982da07a883c2'] = '被購產品：';
$_MODULE['<{statssales}prestashop>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'CSV輸出';
$_MODULE['<{statssales}prestashop>statssales_da80af4de99df74dd59e665adf1fac8f'] = '這段期間沒有訂單。';
$_MODULE['<{statssales}prestashop>statssales_bb7ad89807bf69ddca986c142311f936'] = '產品及訂單';
$_MODULE['<{statssales}prestashop>statssales_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Commandes';
$_MODULE['<{statssales}prestashop>statssales_b52b44c9d23e141b067d7e83b44bb556'] = '產品';


return $_MODULE;
