<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = '找不到此頁';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = '導引';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = '404錯誤';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_a90083861c168ef985bf70763980aa60'] = '如何察覺這些錯誤？ ';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = '頁面';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = '參照';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = '計數器';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_d8847bc418fc4f5a3e37c2e8390bb9ed'] = '空資料庫';


return $_MODULE;
