
[{shop_url}] 

{lastname}{firstname} 您好, 

{reply} 

您可以查看您的訂單，並從這裡下載您的發票"訂單記錄"
[{history_url}]
在您的客戶賬戶點擊"我的賬戶"
[{my_account_url}]。 

如果您是遊客賬戶，您可以在我們店舖的"遊客追踪"
[{guest_tracking_url}] 頁面追踪您的訂單。 

{shop_name} [{shop_url}] powered by
PrestaShop(tm) [http://www.prestashop.com/] 

