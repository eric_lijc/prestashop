
[{shop_url}] 

{lastname}{firstname} 您好,

多謝惠顧 {shop_name}! 

您的訂單{order_name} 已經發貨。

您將很快會收到一個用來追踪您包裹郵寄進度的鏈接。

您可以查看您的訂單，並從這裡下載您的發票"訂單記錄"
[{history_url}]
在您的客戶賬戶點擊"我的賬戶"
[{my_account_url}]。 

{shop_name} [{shop_url}] powered by
PrestaShop(tm) [http://www.prestashop.com/] 

